import pytest
from unittest.mock import patch, MagicMock, call

from main import write_to_file


@pytest.fixture
def fixture_test():
        return patch('builtins.open', MagicMock())


@pytest.mark.parametrize(('name, phrase'),
               [('name1.txt', 'phrase1'),
                ('name2.txt', 'phrase2'),
                ('name3.txt', 'phrase3')])
def test_full_checkup(name, phrase, fixture_test):
    with fixture_test as open_mock:
        write_to_file(name, phrase)
        open_mock.assert_has_calls(calls=[call(name, 'a'), call().__enter__().write(phrase)], any_order=True)


def test_parametrs():
    with pytest.raises(TypeError):
        write_to_file('new_file.txt', 123)
        write_to_file('new_file.txt', [1,2,4,5])
