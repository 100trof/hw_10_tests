def write_to_file(name, phrase):
    if type(phrase) != str:
        raise TypeError
    with open(name, 'a') as file_1:
        file_1.write(phrase)

